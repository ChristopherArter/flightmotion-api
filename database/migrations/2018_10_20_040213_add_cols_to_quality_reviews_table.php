<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToQualityReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quality_reviews', function (Blueprint $table) {
            $table->string('serial_number')->nullable();
            $table->string('reference_number')->nullable();
            $table->string('part_number')->nullable();
            $table->json('requirements')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quality_reviews', function (Blueprint $table) {
            //
        });
    }
}
