<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('open');
            $table->boolean('approved')->default(false);
            $table->date('approved_on')->nullable();
            $table->boolean('complete')->default(false);
            $table->boolean('drop_ship')->nullable();
            $table->integer('mro_id')->nullable();
            $table->decimal('cost', 8, 2)->nullable();
            $table->string('lead_time')->nullable();
            $table->integer('customer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
