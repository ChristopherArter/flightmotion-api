<?php 

return [

    'trace_types'   =>  [
        'OEM'       =>  'OEM',
        '145'       =>  '145',
        '129'       =>  '129',
        '121'       =>  '121',
        'Foreign'   =>  'Foreign?',
        '135'       =>  '135',
        'Military'  =>  'Military',
        'Other'     =>  'Other',
    ],

    'item_statuses' =>  [

        'in-stock'      =>  'In-stock',
        'on-lead'       =>  'On Lead',
        'repair'        =>  'Out for Repair',
        'transit'       =>  'In Transit',
        'delivered'     =>  'Delivered to Customer',
        'rma'           =>  'Returned',
        'quarantine'    =>  'In Quarantine'
    ],

    'qc_review_statuses' =>  [

        'in-review' => 'In Review',
        'information-requested' => 'Information Requested',
        'failed'    =>  'Failed',
        'approved'  =>  'Approved'
    ],

    'conditions'    =>  [
        'NE'    =>  'NE',
        'NS'    =>  'NS',
        'OH'    =>  'OH',
        'SV'    =>  'SV',
        'AR'    =>  'AR',

    ],
    'email_addresses' =>  [
        'updates' =>  'updates@flightmotionav.com',
        'quotes'    =>  'rfq@flightmotionav.com',
        'sales'     =>  'sales@flightmotionav.com'
    ],

    'brand' =>  [
        'blue'  =>  '#371e5d'
    ],

    'location'  =>  [
        'address'   =>  '846 Jaybee Ave, Davenport FL 33897',
        'phone' =>  '555-555-5555',
        'email' =>  'sales@flightmotionav.com'
    ],

    'prefixes'  =>  [
        'order'             =>  'ORD',
        'repair'            =>  'REP',
        'quote'             =>  'QUO',
        'inventory_item'    =>  'ITY',
    ]
];