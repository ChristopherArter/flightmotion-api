<?php 

return [
    'realm_id'  =>  env('QUICKBOOKS_REALM_ID'),
    'client'    =>  env('QUICKBOOKS_CLIENT_ID'),
    'secret'    =>  env('QUICKBOOKS_SECRET'),

];