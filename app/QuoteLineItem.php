<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contact;
use App\Company;
use App\Part;
use App\Quote;
use App\InventoryItem;


class QuoteLineItem extends Model
{
    
    protected $table = 'quote_line_items';
    
    public function quote(){
        return $this->belongsTo('App\Quote');
    }

    public function part(){
        return $this->belongsTo('App\Part');
    }

    public function inventoryItem(){
        return $this->belongsTo('App\InventoryItem');
    }

}
