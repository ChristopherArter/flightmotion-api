<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use PDF;

class CreatePdf implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $options;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->options['data'];
        $view = $this->options['view'];
        $path = $this->options['path'];

        $pdf = PDF::loadView($view, ['data' => $data] );
        Storage::disk('s3-public')->put($path, $pdf->output()); 
    }
}
