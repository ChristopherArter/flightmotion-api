<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShareAssetsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $parts;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($collection)
    {
        $this->parts = $collection;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->from(config('flightmotion.email_addresses.sales'))
        ->view('mail.assetlist');
    }
}
