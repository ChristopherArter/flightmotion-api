<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repair;
use App\Company;
use App\Comment;
use App\Shipment;
use App\QualityReview;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    public function repairs(){
        return $this->hasMany('App\Repair');
    }

    public function customer(){
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function vendor(){
        return $this->belongsTo('App\Company', 'vendor_id');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function qualityReviews(){
        return $this->hasMany('App\QualityReview');
    }

    public function shipments(){
        return $this->morphMany('App\Shipment', 'shipmentable');
    }

    /**
     * Determine the QC status for the Order
     *
     * @return void
     */
    public function qualityStatus(){
        if($this->type == 'purchase'){

            if( $this->qualityReviews->count() > 0 ){
                foreach($this->qualityReviews as $review)
                {
                    if(! $review->approved){
                        return 'unapproved';
                    }
                }
                return 'approved';
            } else {
                return 'unapproved';
            }
        } else {
            return 'NA';
        }
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return 'https://hooks.slack.com/services/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';
    }
}
