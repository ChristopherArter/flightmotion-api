<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RequestForQuote;
use App\Quote;
use App\InventoryItem;

class Part extends Model
{
    protected $table = 'parts';
    public $casts = [
        'nhas'      =>  'array',
        'manuals'   =>  'array'
    ];
    /**
     * RFQ eloquent relationship
     *
     * @return void
     */
    public function rfqs(){
        return $this->belongsToMany('App\RequestForQuote');
    }

    /**
     * Explicit method (for some reason?)
     *
     * @return void
     */
    public function RequestForQuotes(){
        return $this->rfqs();
    }

    /**
     * Quote eloquent relationship
     *
     * @return void
     */
    public function quotes(){
        return $this->hasMany('App\Quote');
    }

    /**
     * Latest Price method
     *
     * @return void
     */
    public function latestPrice(){
        $latestQuoteItem = $this->quotes->first();
        return $latestQuoteItem->amount;
    }

    /**
     * InventoryItem eloquent relationship.
     *
     * @return void
     */
    public function inventoryItems(){
        return $this->hasMany('App\InventoryItem');
    }

    public function mros(){
        return $this->belongsToMany('App\Company')->wherePivot('type', 'repair');
    }
}
