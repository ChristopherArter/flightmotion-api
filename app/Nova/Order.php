<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\MorphMany;
use App\Nova\Company;


use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Order';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'order_number';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'order_number',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Order Number')->withMeta(['extraAttributes' => [
                'readonly' => true
            ]])->hideWhenUpdating()
            ->hideWhenCreating(),
            Date::make('Created', 'created_at')
            ->hideWhenUpdating()
            ->hideWhenCreating(),
            Select::make('Type','type')->options(
                [
                    'purchase'  =>  'purchase',
                    'sale'  =>  'sale',
                    'exchange'  =>  'exchange'
                ]
            )->sortable(),
            
            /**
             * Removed Parts
             */

             /**
              * Customer
              */
            BelongsTo::make('Customer', 'customer', Company::class)->searchable()
            ->help('CUSTOMER'),

            /**
             * Vendor
             */
            BelongsTo::make('Vendor', 'vendor', Company::class)->searchable()
            ->help('VENDOR'),

            HasMany::make('Repairs'),
            
            MorphMany::make('Shipments'),
            HasMany::make('Quality Reviews', 'qualityReviews'),
            MorphMany::make('Comments'),
            
                        /**
            * Description
            */
            Text::make('QC Status', function(){
                return $this->qualityStatus(); })->withMeta(['extraAttributes' => [
            'readonly' => true
        ]]),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
