<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsToMany;
use App\Nova\Part;

class RequestForQuote extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\RequestForQuote';

    public static $with = ['parts'];
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Part Number', function(){
                if($this->parts->count() > 1){
                    return 'Multiple';
                } else if($this->part_number ) {
                    return $this->part_number;
                } else {
                    $part = $this->parts->first();
                    if($part){
                        return $part->part_number;
                    }
                }
            }),
            Date::make('Date', 'created_at')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),
            Text::make('email')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),
          Boolean::make('Quoted'),
            Select::make('Source', 'source')->options([
                'Web'   =>  'web',
                'Phone' =>  'phone',
                'Email' =>  'email',
                'other' =>  'other'
            ]),

            Textarea::make('Notes', 'notes')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),
            BelongsToMany::make('Parts'),

        ];
    }

            /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'RFQs';
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [ new Filters\RequestStatus ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [ 
            // new Lenses\RequestStatus
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [new Actions\ConvertRfqToQuote];
    }
}
