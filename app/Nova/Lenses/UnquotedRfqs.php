<?php

namespace App\Nova\Lenses;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;

use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Lenses\Lens;
use Laravel\Nova\Http\Requests\LensRequest;

class RequestStatus extends Lens
{
    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->where('quoted', false)
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Part Number', function(){
                if($this->parts->count() > 1){
                    return 'Multiple';
                } else if($this->part_number ) {
                    return $this->part_number;
                } else {
                    $part = $this->parts->first();
                    if($part){
                        return $part->part_number;
                    }
                }
            }),
            Date::make('Date', 'created_at')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),
            Text::make('email')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),
          Boolean::make('Quoted'),
            Select::make('Source', 'source')->options([
                'Web'   =>  'web',
                'Phone' =>  'phone',
                'Email' =>  'email',
                'other' =>  'other'
            ]),

            Textarea::make('Notes', 'notes')->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),

        ];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'unquoted-rfqs';
    }
}
