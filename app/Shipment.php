<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TrackingEvent;
use App\InventoryItem;

class Shipment extends Model
{

    public $with = ['trackingEvents'];

    public function trackingEvents(){
        return $this->hasMany('App\TrackingEvent');
    }

    public function inventoryItems(){
        return $this->belongsToMany('App\InventoryItem');
    }
    public function shipmentable()
    {
        return $this->morphTo();
    }
}
