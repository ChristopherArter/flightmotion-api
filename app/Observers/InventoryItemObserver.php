<?php

namespace App\Observers;

use App\InventoryItem;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class InventoryItemObserver
{
    /**
     * Handle the inventory item "created" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function created(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "updated" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function updated(InventoryItem $inventoryItem)
    {
 
        // $this->pushToFirebase($inventoryItem);
    }

    /**
     * Handle the inventory item "deleted" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function deleted(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "restored" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function restored(InventoryItem $inventoryItem)
    {
        //
    }

    /**
     * Handle the inventory item "force deleted" event.
     *
     * @param  \App\InventoryItem  $inventoryItem
     * @return void
     */
    public function forceDeleted(InventoryItem $inventoryItem)
    {
        //
    }

    public function pushToFirebase(InventoryItem $inventoryItem){

        $serviceAccount = ServiceAccount::fromJsonFile(storage_path() . '/flight-motion-firebase-adminsdk-powfm-40b30aa701.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://flight-motion.firebaseio.com')
            ->create();
         
        $database = $firebase->getDatabase();
         
        $newPost = $database
        ->getReference('assets')
        ->push([
            'id' => $inventoryItem->id,
            'model_number' => $inventoryItem->model_number,
            'part_number'   =>  $inventoryItem->part_number,
            'condition' =>  $inventoryItem->condition,
        ]);
    }
}
