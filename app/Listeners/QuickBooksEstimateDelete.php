<?php

namespace App\Listeners;

use App\Events\QuickBooksEstimateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuickBooksEstimateDelete
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QuickBooksEstimateEvent  $event
     * @return void
     */
    public function handle(QuickBooksEstimateEvent $event)
    {
        //
    }
}
