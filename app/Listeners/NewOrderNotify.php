<?php

namespace App\Listeners;

use App\Events\NewOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NewOrderNotification;

class NewOrderNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewOrder  $event
     * @return void
     */
    public function handle(NewOrder $event)
    {
        $event->order->order_number = 'ORD-' . strtoupper( str_random(6) );
        $event->order->save();
        $event->order->notify(new NewOrderNotification($event->order));
    }
}
