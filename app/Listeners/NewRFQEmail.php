<?php

namespace App\Listeners;

use App\Events\NewRFQ;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use \App\Mail\NewRFQMail;

class NewRFQEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRFQ  $event
     * @return void
     */
    public function handle(NewRFQ $event)
    {
        Mail::to('carter@flightmotionav.com')->send(new NewRFQMail($event->requestForQuote));
    }
}
