<?php

namespace App\Listeners;

use App\Events\NewOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\QualityReview;

class NewOrderCreateQcReviews
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewOrder  $event
     * @return void
     */
    public function handle(NewOrder $event)
    {
        if($event->order->type == 'purchase')
        {
            $event->order->qualityReviews()->createMany([
                [
                    'name'  =>  'Material Cert',
                    'priority'  =>  1,
                    'requirements'  =>  [
                        'requirement'   =>  'Statement of non incident',
                        'requirement'   =>  'MSN, Serial or reference #',
                        'requirement'   =>  'Removal Tag',
                    ]
                ],
                [
                    'name'  =>  'LLP Back to Birth',
                    'priority'  =>  1,
                    'requirements'  =>  [
                        'requirement'   =>  'Log cards',
                        'requirement'   =>  'Original 8130',
                    ]
                ],
                [
                    'name'  =>  'Transfers of Ownership',
                    'priority'  =>  1,
                    'requirements'  =>  [
                        'requirement'   =>  'Packing Slips',
                        'requirement'   =>  'Material Certs',
                        'requirement'   =>  'Removal Tags',
                    ]
                ],
            ]);
        }
    }
}
