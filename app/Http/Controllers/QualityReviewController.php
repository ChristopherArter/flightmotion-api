<?php

namespace App\Http\Controllers;

use App\QualityReview;
use Illuminate\Http\Request;

class QualityReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QualityReview  $qualityReview
     * @return \Illuminate\Http\Response
     */
    public function show(QualityReview $qualityReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QualityReview  $qualityReview
     * @return \Illuminate\Http\Response
     */
    public function edit(QualityReview $qualityReview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QualityReview  $qualityReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QualityReview $qualityReview)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QualityReview  $qualityReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualityReview $qualityReview)
    {
        //
    }
}
