<?php

namespace App\Http\Controllers;

use App\PackingSlip;
use Illuminate\Http\Request;

class PackingSlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PackingSlip  $packingSlip
     * @return \Illuminate\Http\Response
     */
    public function show(PackingSlip $packingSlip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackingSlip  $packingSlip
     * @return \Illuminate\Http\Response
     */
    public function edit(PackingSlip $packingSlip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackingSlip  $packingSlip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackingSlip $packingSlip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackingSlip  $packingSlip
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackingSlip $packingSlip)
    {
        //
    }
}
