<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';
    /**
     * Part eloqunent relationship.
     *
     * @return void
     */
    public function part(){
        return $this->belongsTo('App\Part');
    }
}
