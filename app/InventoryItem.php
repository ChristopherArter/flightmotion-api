<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Part;
use App\Quote;
use App\Warehouse;
use App\QuoteLineItem;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Laravel\Nova\Actions\Actionable;
use App\Repair;
use App\Shipment;

class InventoryItem extends Model implements HasMedia
{
    use Actionable, HasMediaTrait;

    //protected $with = ['removedFrom'];
    protected $with = ['part'];
    public $appends = ['trace', 'trace_type'];

    public function part(){
        return $this->belongsTo('App\Part');
    }

    public function quoteLineItems(){
        return $this->hasMany('App\QuoteLineItem');
    }

    public function quotes(){
        return $this->hasManyThrough('App\Quote', 'App\QuoteLineItem');
    }

    public function warehouse(){
        return $this->belongsTo('App\Warehouse');
    }

    public function removedFrom(){
        return $this->belongsTo('App\InventoryItem', 'removed_from_id');
    }

    public function removedParts(){
        return $this->hasMany('App\InventoryItem', 'removed_from_id');
    }

    public function repairs(){
        return $this->belongsToMany('App\Repair');
    }

    public function shipments(){
        return $this->belongsToMany('App\Shipment');
    }

    public function galleries(){
        return $this->morphMany('App\Gallery', 'galleryable');
    }

    public function documents(){
        return $this->morphMany('App\Document', 'documentable');
    }

    public function registerMediaConversions(Media $media = null){
    $this->addMediaConversion('thumb')
        ->width(130)
        ->height(130);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('my_multi_collection');
    }

    public function getTraceAttribute(){
        if($this->removedFrom && $this->inherit_trace ){
            return $this->removedFrom->trace;
        } else {
            return $this->item_trace;
        }
    }

    public function getTraceTypeAttribute(){
        
        if($this->removedFrom && $this->inherit_trace ){
            return $this->removedFrom->item_trace_type;
        } else {
            return $this->item_trace_type;
        }
    }

    public function createDocuments($template)
    {
        if($template == 'asset')
        {
            ( new Document )->createMinipack($this);
            ( new Document )->createMaterialCert($this);
            ( new Document )->createTracekit($this);
        }
    }
}
