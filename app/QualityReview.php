<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\User;
use App\Purchase;
use Laravel\Nova\Actions\Actionable;

class QualityReview extends Model
{
    use Actionable;
    protected $casts = [
        'requirements'  =>  'array'
    ];

    protected $fillable = [
        'requirements', 'priority', 'name'
    ];

    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function purchase(){
        return $this->belongsTo('App\Purchase');
    }

    public function approve(){
        $this->approved = true;
        $this->status = 'approved';
        $this->save();
    }
}
