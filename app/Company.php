<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contact;
use App\Order;
class Company extends Model
{
    protected $table = 'companies';

    public function contacts(){
        return $this->hasMany('App\Contact');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function capabilities(){
        return $this->belongsToMany('App\Part')
        ->wherePivot('type', 'repair');
    }

}
