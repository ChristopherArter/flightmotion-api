<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Observers\OrderObserver;
use App\Order;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\NewRFQ' => [
            'App\Listeners\NewRFQEmail',
            'App\Listeners\NewRFQAutoQuote',
        ],

        'App\Events\QuickBooksEstimateEvent' => [
            'App\Listeners\QuickBooksEstimateCreate',
            'App\Listeners\QuickBooksEstimateUpdate',
            'App\Listeners\QuickBooksEstimateDelete',
            'App\Listeners\QuickBooksEstimateMerge',
        ],

        'App\Events\QuickBooksItemEvent' => [
            'App\Listeners\QuickBooksItemCreate',
            'App\Listeners\QuickBooksItemUpdate',
            'App\Listeners\QuickBooksItemDelete',
            'App\Listeners\QuickBooksItemMerge',
        ],
        'App\Events\NewOrder' => [
            'App\Listeners\NewOrderCreateQcReviews',
            'App\Listeners\NewOrderNotify',
            // 'App\Listeners\NewOrderShipmentSetup',
            

        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        

        //
    }
}
