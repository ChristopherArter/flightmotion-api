<?php
use App\Part;
use App\Company;
use App\Contact;
use App\InventoryItem;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes 
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\RequestForQuote;

Route::get('/', function () {
    return view('welcome');
});

Route::get('company-test', function(){
    // $company = Company::find(1);
    // $company->load('capabilities');
    // return $company;
    $part = Part::find(1);
    $part->load('mros');
    return $part;
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/inventory/{id}', 'InventoryItemController@show')->name('inventoryitems.show');


// Route::get('/epcor', function(){
// $rows = array_map('str_getcsv', file( public_path() . '/parts.csv') );

// $epcor = Company::find(506);

// $rowArray = collect();
// foreach($rows as $row){
//     if(count($row) > 20){


//         $findPart = Part::where('part_number', $row[1])->get();

//         if($findPart->isEmpty()){

//             $part = new Part();
//             $part->part_number = $row[1];
//             $part->description = $row[0];
//             $part->manuals = [
//                 'cmm'   =>  $row[5]
//             ];
//             $part->oem = $row[6];

//             $part->save();

//             $epcor->capabilities()->save($part, ['type' => 'repair']);
//         }

//     }
//     // 27,478 rows
//     // //make a new part 
//     // if(isset($row[1])){

//     //     $findPart = Part::where('part_number', $row[1])->get();

//     //     // no part found, lets make a new one.
//     //     if($findPart->isEmpty()){

//             // $part = new Part();
//             // $part->part_number = $row[1];
//             // $part->description = $row[0];
//             // $part->manuals = [
//             //     'cmm'   =>  $row[5]
//             // ];

//     //     } else {
//     //         // we found a part, lets use this;
//     //         $part = $findPart;
//     //     }

//     //     return $part;

//     //     $rowArray->push($part);

//     //     /**
//     //      * 0 - description
//     //      * 1 - main pn
//     //      * 2 - c/p*
//     //      * 3 - sub P/N
//     //      * 4 - NHA P/N
//     //      * 5 - CMM
//     //      * 6 - OEM
//     //      * 7 - BCAG pn
//     //      * 8 - A319/A320/A321
//     //      * 9 - A330/A340 
//     //      * 10 - B737CL
//     //      * 11 - 737NG
//     //      * 12 - B747CG
//     //      * 13 - B747-400
//     //      * 14 - B757
//     //      * 15 - B767
//     //      * 16 - B777
//     //      * 17 - EMB170/175 EMB 190/195
//     //      * 18 - B787
//     //      * 19 - engine_application
//     //      * 20 - limitations
//     //      */
        
//     // }
    
// }

// $epcor->load('capabilities');
// return $epcor;

// });

Route::get('item/{id}', function($id){
    $item = InventoryItem::find($id);
    return $item->removedFrom;
});

Route::get('removal/{id}/', function ($id){
    $data = InventoryItem::find($id);
    $data->with('removedFrom')->with('part');
    return view('pdf.removal_tag')->with(['data' => $data]);
});