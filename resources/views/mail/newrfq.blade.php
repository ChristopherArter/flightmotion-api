@extends('../layouts/mail')


@section('content')
<!-- 2 column layout with 10px spacing -->
<table width="600" cellpadding="0" cellspacing="0">
  <tr>
    <td width="200">
      <p>Part Number:</p>
    </td>
    <td width="10">&nbsp;</td>
    <td width="390">
     <b>{{ $requestForQuote->part_number }}</b>
    </td>
  </tr>

    @if($requestForQuote->conditions)

    <tr>
    <td width="200">
      <p>Conditions:</p>
    </td>
    <td width="10">&nbsp;</td>
    <td width="390">
        <ul>
        @foreach($requestForQuote->conditions as $condition)
            <li>{{ $condition }}</li>
        @endforeach
        </ul>
    </td>
  </tr>

    @endif
    <tr>
    <td width="200">
      <p>Email:</p>
    </td>
    <td width="10">&nbsp;</td>
    <td width="390">
     <b>{{ $requestForQuote->email }}</b>
    </td>
  </tr>

    <tr>
    <td width="200">
      <p>Email:</p>
    </td>
    <td width="10">&nbsp;</td>
    <td width="390">
     <b>{{ $requestForQuote->part_number }}</b>
    </td>
  </tr>

    <tr>
    <td width="200">
      <p>Details:</p>
    </td>
    <td width="10">&nbsp;</td>
    <td width="390">
     <b>{{ $requestForQuote->notes }}</b>
    </td>
  </tr>
</table>

@if($requestForQuote->parts)
{{ $requestForQuote->parts }}
@endif
@endsection