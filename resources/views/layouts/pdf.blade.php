<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>@yield('page_title')</title>

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>

</head>
<body>

    <div class="container-fluid" style="padding:20px;">
            <div class="row no-gutters" style="margin-bottom:30px;">
                    <div class="col-6">
                        <p><strong>Flight Motion</strong></p>
                        <span>{{ config('flightmotion.location.address') }}</span>
                        <br>
                        <span>{{ config('flightmotion.location.phone') }}</span>
                        <br>
                        <span>{{ config('flightmotion.location.email') }}</span>
                        {{--  <img src="{{ url('/flight-motion-logo-light.png') }}">  --}}
                    </div>
                    <div class="col-6">
                        
                            <h2 class="float-right">@yield('title')</h2>

                    </div>
                </div>

            @yield('content')
    </div>
</body>
</html>